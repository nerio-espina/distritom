package ar.com.distritomoda.dm

import ImagePicker.TEMP_IMAGE_NAME
import android.R.attr.*
import android.app.Fragment
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.transition.Fade
import android.view.View
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Matrix
import android.provider.MediaStore
import kotlinx.android.synthetic.main.fragment_captura.*
import android.R.*
import android.app.AlertDialog
import android.content.Context
import kotlinx.android.synthetic.main.fragment_describe.*
import android.view.inputmethod.InputMethodManager

import android.support.v4.widget.PopupWindowCompat.showAsDropDown
import android.content.DialogInterface
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Environment
import android.widget.*
import android.os.Environment.DIRECTORY_PICTURES
import android.support.v4.content.FileProvider
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class AgregarProducto : AppCompatActivity(), codigoproducto.codigocom, captura.capturacom, nombrearticulo.nombrecom, describe.describecom {
    //Declaramos el bitmap de sin foto
    var mCurrentPhotoPath: String = ""
    // Declaraciones de los Fragmentos
    private var F0: captura? = null
    private var F1: describe? = null
    private var F2: detalle? = null
    //private var F2: codigoproducto? = null
    //private var F3: describe? = null
    private var screenindex: Int = 0 //Definimos un entero para obtener el Fragmento en el que estamos y poder regresar al anterior en caso que aplique
    //Declaraciones de Parametros para la captura de las imagenes
    var PICK_IMAGE = 1
    val REQUEST_TAKE_PHOTO = 1
    var numimagen = 0

    override fun onclick(indice: Int) {
        when(indice){
            0 -> showfragment(F0 as android.support.v4.app.Fragment)
            1 -> showfragment(F1 as android.support.v4.app.Fragment)
            2 -> showfragment(F2 as android.support.v4.app.Fragment)
            //2 -> showfragment(F2 as android.support.v4.app.Fragment)
            //3 -> showfragment(F3 as android.support.v4.app.Fragment)
        }
        screenindex = indice
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(currentFocus?.getWindowToken(), 0)
    }

    override fun onTextChange(txtcampo: EditText) {
        when(txtcampo.id){
            txtNombre.id ->
                chars_nombre.text = (60-txtNombre.length()).toString()
            txtDescripcion.id ->
                chars_descripcion.text = (60-txtDescripcion.length()).toString()
            txtCodigo.id ->
                chars_codigo.text = (60-txtCodigo.length()).toString()
        }
    }

    override fun onEditBoxEdited(v: TextView?, boxContent: String) {
        //v?.text = (60-txtnombre.length()).toString()
    }
    override fun validardescripcion(txtcampo: EditText) {

        when(txtcampo.id){
            txtNombre.id ->
                if(txtNombre.text.isEmpty()){
                    Toast.makeText(this, getString(R.string.COnombre), Toast.LENGTH_SHORT).show()
                    return
                }
                else{
                    txtCodigo.requestFocus()
                }
            txtCodigo.id -> txtDescripcion.requestFocus()
            txtDescripcion.id -> onclick(2)
        }
    }
    fun showfragment(fra: android.support.v4.app.Fragment){
        val transaction = supportFragmentManager.beginTransaction()
        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
        transaction.replace(R.id.contenedor, fra)
        transaction.commit()
    }

    override fun pasarfotos(img1: Bitmap?, img2: Bitmap?, img3: Bitmap?, img4: Bitmap?, img5: Bitmap?, img6: Bitmap?) {
        //Esta Funcion debe guardar los datos, validar que existan las fotos y continuar al paso de Descripcion
        onclick(1)
    }
    fun remove(v: ImageView?) {
        // Esta funcion elimina la foto del recuadro y resetea el Tag a 0 para poder montar otra nueva o dejarla en blanco
        v?.setImageResource(R.drawable.imgtakegrid)
        v?.tag = "0"
    }

    fun takefrontal(v: View){
        //Esta funcion decide la accion para tomar la foto o mostrar el menu de acciones en caso que ya se tenga una foto en el recuadro donde se hace click
        numimagen = numerocaja(v)

        if(v.tag == "0") {
            onPickImage()
        }
        else{
            cuadrodialogo(v as ImageView)
        }
    }
    fun numerocaja(v: View?): Int{
        when(v?.id){
            imggrid1.id -> return 1
            imggrid2.id -> return 2
            imggrid3.id -> return 3
            imggrid4.id -> return 4
            imggrid5.id -> return 5
            imggrid6.id -> return 6
        }
        return 0
    }
    fun cuadrodialogo(v: ImageView?){

        val listItems: Array<String>
        listItems = getResources().getStringArray(R.array.ctxfotoitems)
        val mBuilder = AlertDialog.Builder(this)
        mBuilder.setTitle(getString(R.string.ctxfotosel))
        mBuilder.setItems(listItems, DialogInterface.OnClickListener { dialogInterface, i ->

            when(listItems[i]) {
                "Eliminar" -> remove(v)
                "Sustituir" -> {
                    numimagen = numerocaja(v)
                    onPickImage()
                }

            }
            //mResult.setText(listItems[i])
            dialogInterface.dismiss()
        })

        val mDialog = mBuilder.create()
        mDialog.show()
    }
    var PICK_IMAGE_ID = 1
    fun onPickImage() {
        try{
            var chooseImageIntent = ImagePicker.getPickImageIntent(this) as Intent
            startActivityForResult(chooseImageIntent, PICK_IMAGE)
        }
        catch (ex: Exception){
        }
    }
    fun dispatchTakePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (takePictureIntent.resolveActivity(packageManager) != null) {
            //startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
        }
    }


    // @Throws(IOException::class)
    private fun imagename(): String {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMddHHmmss").format(Date())
        val imageFileName = "DM" + timeStamp
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)

        return storageDir.toString()

    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK) {
            //  Nos aseguramos que el resultado sea correcto

            when (requestCode) {
                PICK_IMAGE -> {
                    var imagenres: Bitmap
                    imagenres = ImagePicker.getImageFromResult(this, resultCode, data) as Bitmap
                    if(imagenres.width < imagenres.height){
                        //Si la imagen existe asignamos el Bitmap resultado a la vista segun la variable numimagen
                        when(numimagen) {
                            1 -> {
                                imggrid1.setImageBitmap(imagenres)
                                imggrid1.tag = "1";
                            }
                            2 -> {
                                imggrid2.setImageBitmap(imagenres)
                                imggrid2.tag = "1";
                            }
                            3 -> {
                                imggrid3.setImageBitmap(imagenres)
                                imggrid3.tag = "1";
                            }
                            4 -> {
                                imggrid4.setImageBitmap(imagenres)
                                imggrid4.tag = "1";
                            }
                            5 -> {
                                imggrid5.setImageBitmap(imagenres)
                                imggrid5.tag = "1";
                            }
                            6 -> {
                                imggrid6.setImageBitmap(imagenres)
                                imggrid6.tag = "1";
                            }
                        }

                    }
                    else{
                        val toast = Toast.makeText(applicationContext, getString(R.string.imagenvertical), Toast.LENGTH_LONG)
                        toast.show()
                    }

                }
                else -> {
                }
            }
        }
    }

    fun Bitmap.rotate(degrees: Float): Bitmap {
        // Esta funcion es capaz de rotar un Bitmap directamente desde el objeto,
        val matrix = Matrix().apply { postRotate(degrees) }
        return Bitmap.createBitmap(this, 0, 0, width, height, matrix, true)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_agregar_producto)
        //Creamos las instancias de los fragmentos que deseamos cargar en esta actividad
        F0 = captura.newInstance()
        F1 = describe.newInstance()
        F2 = detalle.newInstance()

        showfragment(F0 as android.support.v4.app.Fragment)

    }


    override fun onPause() {
        super.onPause()
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(currentFocus?.getWindowToken(), 0)
    }
}



package ar.com.distritomoda.dm

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_codigoproducto.*


class codigoproducto : Fragment() {

    var mCallback: codigocom? = null
    interface codigocom{
        fun onclick(indice: Int)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_codigoproducto, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnnext2.setOnClickListener{
            mCallback?.onclick(3)
        }
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onDetach() {
        super.onDetach()
        mCallback = null
    }

    override fun onStart() {
        super.onStart()
        try {
            mCallback = activity as codigocom
        } catch (e: ClassCastException) {
            throw ClassCastException(e.message)
        }

    }

    companion object {

        fun newInstance(): codigoproducto {
            val fragment = codigoproducto()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }
}

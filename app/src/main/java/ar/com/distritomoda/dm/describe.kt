package ar.com.distritomoda.dm

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_describe.*
import android.text.Editable
import android.text.TextWatcher
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText


class describe : Fragment(){

    var mCallback: describecom? = null

    interface describecom {
        fun onTextChange(txtcampo: EditText)
        fun validardescripcion(txtcampo: EditText)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val vista = inflater!!.inflate(R.layout.fragment_describe, container, false) as View

        return vista
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        txtDescripcion.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                mCallback?.onTextChange(txtDescripcion)
            }
        })

        txtCodigo.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                mCallback?.onTextChange(txtCodigo)
            }
        })
        txtNombre.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                mCallback?.onTextChange(txtNombre)
            }
        })
        txtNombre.setOnEditorActionListener { _, actionId, _ ->
            var handled = false
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                mCallback?.validardescripcion(txtNombre)
                handled = true
            }
            handled
        }
        txtCodigo.setOnEditorActionListener { _, actionId, _ ->
            var handled = false
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                mCallback?.validardescripcion(txtCodigo)
                handled = true
            }
            handled
        }
        txtDescripcion.setOnEditorActionListener { _, actionId, _ ->
            var handled = false
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                mCallback?.validardescripcion(txtDescripcion)
                handled = true
            }
            handled
        }

        val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, InputMethodManager.HIDE_IMPLICIT_ONLY)
        imm.hideSoftInputFromWindow(view?.getWindowToken(), 0)

        txtNombre.requestFocus()
    }
    override fun onAttach(context: Context?) {
        super.onAttach(context)


    }

    override fun onStart() {
        super.onStart()
        try {
            mCallback = activity as describecom

        } catch (e: ClassCastException) {
            throw ClassCastException(context.toString() + " must implement OnHeadlineSelectedListener")
        }

    }
    override fun onDetach() {
        super.onDetach()
        mCallback = null
    }



    companion object {

        fun newInstance(): describe {
            val fragment = describe()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }
}

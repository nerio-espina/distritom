package ar.com.distritomoda.dm

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.Message
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import kotlinx.android.synthetic.main.fragment_captura.*
import android.widget.Toast
import android.provider.MediaStore
import android.content.Intent
import android.content.pm.PackageInfo
import android.graphics.Bitmap
import android.app.Activity.RESULT_OK
import android.media.Image
import android.widget.ImageView
import android.view.MenuInflater
import android.view.ContextMenu.ContextMenuInfo
import android.view.ContextMenu



class captura : Fragment() {

    var mCallback: capturacom? = null
    interface capturacom{
        fun pasarfotos(img1: Bitmap?, img2: Bitmap?, img3: Bitmap?, img4:Bitmap?, img5: Bitmap?, img6: Bitmap?)
   }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_captura, container, false)

    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnnext.setOnClickListener{
            mCallback?.pasarfotos(null, null, null, null, null, null)
        }
        imggrid1.tag == false
        imggrid2.tag == false
        imggrid3.tag == false
        imggrid4.tag == false
        imggrid5.tag == false
        imggrid6.tag == false
        //imggrid1.setOnClickListener{mCallback?.remove(1)}
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

    }

    override fun onStart() {
        super.onStart()
        try {
            mCallback = activity as capturacom
        } catch (e: ClassCastException) {
            throw ClassCastException(context.toString() + " must implement OnHeadlineSelectedListener")
        }
    }
    override fun onDetach() {
        super.onDetach()
        mCallback = null
    }

    companion object {

        fun newInstance(): captura {
            val fragment = captura()
            val args = Bundle()

            fragment.arguments = args
            return fragment
        }
    }
}


package ar.com.distritomoda.dm

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import kotlinx.android.synthetic.main.fragment_nombrearticulo.*
import android.widget.EditText
import android.widget.Toast
import android.view.inputmethod.EditorInfo
import android.widget.TextView.OnEditorActionListener


class nombrearticulo : Fragment() {

    public var mCallback: nombrecom? = null

    public interface nombrecom {
        fun onEditBoxEdited(v: TextView?, boxContent: String)
        fun onclick(indice: Int)
    }

    fun FragA(mCallback: nombrecom) {
        this.mCallback = mCallback
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var V: View = inflater.inflate(R.layout.fragment_nombrearticulo, container, false)
        return V
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val et = view?.findViewById<View>(R.id.txtnombre)
        (et as EditText).addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun afterTextChanged(s: Editable) {
                mCallback?.onEditBoxEdited(chars_numeros, s.toString())
            }
        })

        et.setOnEditorActionListener { _, actionId, _ ->
            var handled = false
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                validar()
                handled = true
            }
            handled
        }

        btnnext2.setOnClickListener{validar()}

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

    }
    fun validar(){
        if(txtnombre.text.isEmpty()){
            var msg = Toast.makeText(context,"Debe ingresar los datos solicitados", Toast.LENGTH_SHORT)
            msg.show()
        }
        else{
            mCallback?.onclick(2)
        }

    }

    override fun onStart() {
        super.onStart()
        try {
            mCallback = activity as nombrecom
        } catch (e: ClassCastException) {
            throw ClassCastException(e.message + " must implement OnHeadlineSelectedListener")
        }
    }
    override fun onDetach() {
        super.onDetach()
        mCallback = null
    }

    companion object {
        fun newInstance(): nombrearticulo {
            val fragment = nombrearticulo()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }
}
